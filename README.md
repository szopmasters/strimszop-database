- Temat projektu: 
    - Sklep ze sprzedażą streamingową

- Skład grupy:
    - Marcin Barzyk i Piotr Sołowiej

- Opis ogólny: 
    - Projekt zakłada stworzenie sklepu, w którym użytkownicy będą mogli dodawać listę przedmiotów, które 
    aktualnie chcą sprzedać. Ponadto, poprzez streaming (zewnętrzne API) będą mogli prezentować swoje produkty na żywo.
    Potencjalni kupujący będą mogli dołączać do pokoju streamu i poprzez czat prowadzić konwersację ze sprzedającym i zadawać 
    pytania dotyczące produktów oraz ew. negocjację ceny.
    
- Funkcjonalności: 
    - Użytkownikowi niezalogowanemu aplikacja umożliwia jedynie obserwację poszczególnych streamów, bez interakcji.
    - Po zalogowaniu się (rejestracja publiczna, najlepiej z wysyłaniem maila weryfikującego + CAPTCHA + API streamingowe wymaga autoryzacji OAuth2)
      - User może: przeglądać streamy, pisać na czacie, kupować przedmioty, subskrybować ulubionych sprzedawców, aby dostawać powiadomienia o streamach,
    wysyłać prywatne wiadomości do innych Userów, zgłaszać użytkowników za nieprzyzwoitą treść oraz samemu zakładać streamy i dodawać do nich listę sprzedawanych produktów z krótkim opisem.
      - Moderator może dodatkowo: mutować/banować użytkowników (do których listy ma dostęp, dodatkowo ma listę zgłoszonych Userów) na określoną ilość czasu na konkretny stream, bądź na całą aplikację, usuwać streamy.
      - Admin może nadawać Userom rolę Moderatora i usuwać użytkowników.
      
    - Sortowanie streamów po kategoriach i po popularności (userzy z największą liczbą subskrypcji/oglądających w danym momencie)
      
    Dylemat: czy dzielić streamy na konkretne kategorie produktów (np. uzytkownik sprzedaje ubrania LUB kosmetyki LUB elektronikę etc.) czy lepiej umożliwić 
    dodawanie wielu kategorii do jednego streama (ograniczonych, żeby User w celu zwiększenia zasięgu nie dodał wszystkich)
    + co ze streamami archiwalnymi?
    
    To zakłada mniej więcej podstawowe funkcjonalności
    
    Dodatkowo: obsługa płatności! bez tego sprzedawcy będą najprawdopodobniej odsyłać kupujących na zewnętrzny portal typu olx.
    
 