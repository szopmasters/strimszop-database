package pl.strimszop.cloud.strimszopdatabase.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.strimszop.cloud.strimszopdatabase.model.product.Invoice;

public interface InvoiceRepository extends JpaRepository<Invoice, Long> {
}
