package pl.strimszop.cloud.strimszopdatabase.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.strimszop.cloud.strimszopdatabase.model.product.Product;
import pl.strimszop.cloud.strimszopdatabase.model.product.ProductPhoto;

public interface ProductPhotoRepository extends JpaRepository<ProductPhoto, Long> {
}
