package pl.strimszop.cloud.strimszopdatabase.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import pl.strimszop.cloud.strimszopdatabase.model.product.Product;
import pl.strimszop.cloud.strimszopdatabase.model.product.ProductCategory;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long>, JpaSpecificationExecutor<Product> {
    List<Product> getAllByNameContainingOrCategoryAndPriceBetween(String name, ProductCategory category, Double from, Double upTo);

    List<Product> getAllByCategory (ProductCategory category);

    List<Product> getAllByNameContaining(String name);

    List<Product> getAllByPriceBetween(Double from, Double upTo);
}
