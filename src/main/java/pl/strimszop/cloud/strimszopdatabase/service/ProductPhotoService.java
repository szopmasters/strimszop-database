package pl.strimszop.cloud.strimszopdatabase.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.strimszop.cloud.strimszopdatabase.model.product.Product;
import pl.strimszop.cloud.strimszopdatabase.model.product.ProductPhoto;
import pl.strimszop.cloud.strimszopdatabase.repository.ProductPhotoRepository;
import pl.strimszop.cloud.strimszopdatabase.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProductPhotoService {
    private final ProductPhotoRepository productPhotoRepository;
    private final ProductRepository productRepository;


    public Optional<ProductPhoto> getById(Long photoId) {
        return productPhotoRepository.findById(photoId);
    }

    public void removeById(Long photoId) {
        productPhotoRepository.deleteById(photoId);
    }

    public void setAsMainPhoto(Long photoId) {
        Optional<ProductPhoto> productPhotoOptional = productPhotoRepository.findById(photoId);
        if (productPhotoOptional.isPresent()) {
            ProductPhoto productPhoto = productPhotoOptional.get();
            Product product = productPhoto.getProduct();

            product.getPhotos().forEach(p -> p.setPrimary(false));
            productPhoto.setPrimary(true);
            productPhotoRepository.save(productPhoto);
            productRepository.save(product);
        }
    }
}
