package pl.strimszop.cloud.strimszopdatabase.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pl.strimszop.cloud.strimszopdatabase.model.account.Account;
import pl.strimszop.cloud.strimszopdatabase.model.product.Product;
import pl.strimszop.cloud.strimszopdatabase.model.product.ProductPhoto;
import pl.strimszop.cloud.strimszopdatabase.model.product.dto.SearchRequest;
import pl.strimszop.cloud.strimszopdatabase.model.specification.ProductSpecification;
import pl.strimszop.cloud.strimszopdatabase.repository.AccountRepository;
import pl.strimszop.cloud.strimszopdatabase.repository.ProductPhotoRepository;
import pl.strimszop.cloud.strimszopdatabase.repository.ProductRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final ProductPhotoRepository productPhotoRepository;
    private final AccountRepository accountRepository;

    public void removeById(Long productId) {
        Optional<Product> productOptional = productRepository.findById(productId);
        if (productOptional.isPresent()) {
            productRepository.deleteById(productId);
        }
    }

    public void add(Product product, Account account, MultipartFile photo) throws IOException {
        if (product.getId() == null) {

            ProductPhoto productPhoto = new ProductPhoto();

            byte[] byteArr = photo.getBytes();

            productPhoto.setPhoto(byteArr);
            productPhoto.setPrimary(true);

            product.setOwner(account);

            productRepository.save(product);
            productPhoto.setProduct(product);
            productPhotoRepository.save(productPhoto);
        } else {
            Optional<Product> optionalProduct = productRepository.findById(product.getId());
            if (optionalProduct.isPresent()) {
                Product product1 = optionalProduct.get();
                product1.setQuantity(product.getQuantity());
                product1.setPrice(product.getPrice());
                product1.setName(product.getName());
                product1.setCategory(product.getCategory());

                productRepository.save(product1);
            }
        }
    }

    public List<Product> getAll(String name) {
        Optional<Account> optionalAccount = accountRepository.findByUsername(name);
        if (optionalAccount.isPresent()) {
            Account userAccount = optionalAccount.get();
            return userAccount.getProducts();
        }
        return new ArrayList<>();
    }

    public Optional<Product> findById(Long id) {
        return productRepository.findById(id);
    }

    public void addPhoto(Long productId, MultipartFile photo) throws IOException {
        Optional<Product> optionalProduct = productRepository.findById(productId);
        if (optionalProduct.isPresent()) {
            Product product = optionalProduct.get();
            ProductPhoto productPhoto = new ProductPhoto();

            byte[] byteArr = photo.getBytes();
            productPhoto.setPhoto(byteArr);

            productRepository.save(product);
            productPhoto.setProduct(product);
            productPhotoRepository.save(productPhoto);
        }
    }

    public List<Product> getAllFromDto(SearchRequest dto) {
        return productRepository.findAll(new ProductSpecification(dto));
    }
}