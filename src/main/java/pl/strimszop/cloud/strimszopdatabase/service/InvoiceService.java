package pl.strimszop.cloud.strimszopdatabase.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.strimszop.cloud.strimszopdatabase.repository.InvoiceRepository;

@Service
@AllArgsConstructor
public class InvoiceService {
    private final InvoiceRepository invoiceRepository;

}
