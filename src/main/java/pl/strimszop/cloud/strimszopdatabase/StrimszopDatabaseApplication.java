package pl.strimszop.cloud.strimszopdatabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StrimszopDatabaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(StrimszopDatabaseApplication.class, args);
    }

}
