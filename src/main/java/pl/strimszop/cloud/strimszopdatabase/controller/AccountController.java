package pl.strimszop.cloud.strimszopdatabase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.strimszop.cloud.strimszopdatabase.model.account.Account;
import pl.strimszop.cloud.strimszopdatabase.model.product.Product;
import pl.strimszop.cloud.strimszopdatabase.service.AccountService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = "/user/")
public class AccountController {

    private AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/register")
    public String registrationForm(Model model, Account account) {
        model.addAttribute("newAccount", account);

        return "registration-form";
    }

    @PostMapping("/register")
    public String register(@Valid Account account, BindingResult result, String passwordConfirm, Model model) {

        if (result.hasErrors()) {
            return registrationError(model, account, result.getFieldError().getDefaultMessage());
        }

        if (!account.getPassword().equals(passwordConfirm)) {
            return registrationError(model, account, "Passwords do not match.");
        }

        if (!accountService.register(account)) {
            return registrationError(model, account, "User with given username already exists.");
        }

        return "redirect:/login";
    }

    private String registrationError(Model model, Account account, String s) {
        model.addAttribute("newAccount", account);
        model.addAttribute("errorMessage", s);

        return "registration-form";
    }

    @GetMapping("/accountInfo")
    public String accountInfo(Model model, Principal principal) {


        Optional<Account> optionalAccount = accountService.getByName(principal.getName());
        if (optionalAccount.isPresent()) {

            model.addAttribute("account", optionalAccount.get());
            return "account-info";
        }
        return "redirect:/";


    }

    @GetMapping("/accountInfo/{id}")
    public String accountInfo(Model model,
                              @PathVariable(name = "id", required = false) Long id) {


        Optional<Account> optionalAccount = accountService.getById(id);
        if (optionalAccount.isPresent()) {

            model.addAttribute("account", optionalAccount.get());
            return "account-info";
        }
        return "redirect:/";
    }


    public String accountInfo(Model model, Account account) {

        Optional<Account> optionalAccount = accountService.getById(account.getId());

        if (optionalAccount.isPresent()) {
            model.addAttribute("photos", optionalAccount.get()
                    .getProducts()
                    .stream()
                    .map(Product::getPhotos)
                    .collect(Collectors.toList()));

            return "account-info";
        }
        return "redirect:/";
    }

}
