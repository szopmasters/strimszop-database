package pl.strimszop.cloud.strimszopdatabase.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.strimszop.cloud.strimszopdatabase.model.account.Account;
import pl.strimszop.cloud.strimszopdatabase.model.product.Product;
import pl.strimszop.cloud.strimszopdatabase.model.product.ProductPhoto;
import pl.strimszop.cloud.strimszopdatabase.service.ProductPhotoService;
import pl.strimszop.cloud.strimszopdatabase.service.ProductService;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Controller
@AllArgsConstructor
@RequestMapping(path = "/photos/")
public class ProductPhotoController {
    private final ProductPhotoService productPhotoService;
    private final ProductService productService;

    @GetMapping("/{photoId}")
    public String getPhoto(Model model, HttpServletRequest request,
                           @PathVariable(name = "photoId") Long photoId) {
        Optional<ProductPhoto> productPhotoOptional = productPhotoService.getById(photoId);
        if (productPhotoOptional.isPresent()) {
            ProductPhoto photo = productPhotoOptional.get();
            model.addAttribute("photo", photo);
            return "photo-view";
        }
        return request.getHeader("referer");
    }

    @GetMapping("/{photoId}/remove")
    public String removePhoto(@PathVariable(name = "photoId") Long photoId) {
        Optional<ProductPhoto> optionalProductPhoto = productPhotoService.getById(photoId);
        Long productId = null;
        if (optionalProductPhoto.isPresent()) {
            productPhotoService.removeById(photoId);
            productId = optionalProductPhoto.get().getProduct().getId();
        }

        return "redirect:/product/" + productId + "/photos";
    }

    @GetMapping("/{photoId}/set")
    public String setPhotoMain(@PathVariable(name = "photoId") Long photoId) {
        productPhotoService.setAsMainPhoto(photoId);


        return "redirect:/product/list";
    }
}
