package pl.strimszop.cloud.strimszopdatabase.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.strimszop.cloud.strimszopdatabase.service.AccountService;

@Controller
@RequestMapping(path = "/admin/")
@PreAuthorize(value = "hasRole('ADMIN')")
public class AdminTaskController {
    @Autowired
    private AccountService accountService;

    @GetMapping("/list")
    public String getTaskListForm(Model model) {
        model.addAttribute("userlist", accountService.getAll());

        return "user-chooser";
    }
}