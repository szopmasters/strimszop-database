package pl.strimszop.cloud.strimszopdatabase.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.strimszop.cloud.strimszopdatabase.model.account.Account;
import pl.strimszop.cloud.strimszopdatabase.model.product.Product;
import pl.strimszop.cloud.strimszopdatabase.model.product.ProductCategory;
import pl.strimszop.cloud.strimszopdatabase.model.product.ProductPhoto;
import pl.strimszop.cloud.strimszopdatabase.model.product.dto.SearchRequest;
import pl.strimszop.cloud.strimszopdatabase.service.AccountService;
import pl.strimszop.cloud.strimszopdatabase.service.ProductService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Controller
@AllArgsConstructor
@RequestMapping(path = "/product/")
public class ProductController {
    private ProductService productService;
    private AccountService accountService;

    @GetMapping(path = "/add")
    public String addProduct(Model model, Product product) {

        model.addAttribute("product", product);
        model.addAttribute("categories", ProductCategory.values());

        return "product-add";
    }

    @PostMapping(path = "/add")
    public String addProduct(Product product, Principal principal, @RequestParam(value = "photo", required = false) MultipartFile photo) {
        Optional<Account> optionalAccount = accountService.getByName(principal.getName());
        if (optionalAccount.isPresent()) {
            try {
                productService.add(product, optionalAccount.get(), photo);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "redirect:/product/list";
    }

    @GetMapping("/edit/{id}")
    public String edit(Model model,
                       @PathVariable(name = "id") Long id) {
        Optional<Product> productOptional = productService.findById(id);
        if (productOptional.isPresent()) {
            Product product = productOptional.get();
            model.addAttribute("product", product);
            model.addAttribute("categories", ProductCategory.values());

            return "product-add";
        }

        return "redirect:/product/list";
    }

    @GetMapping(path = "/remove/{productId}")
    public String removeProduct(@PathVariable(name = "productId") Long productId, HttpServletRequest request) {
        productService.removeById(productId);
        String referer = request.getHeader("referer");

        return "redirect:" + referer;
    }

    @GetMapping("/list")
    public String listProducts(Model model, Principal principal) {
        List<Product> products = productService.getAll(principal.getName());
        model.addAttribute("username", principal.getName());
        model.addAttribute("products", products);
        return "own-product-list";
    }

    @GetMapping("/{productId}/photos")
    public String listPhotos(Model model, Principal principal,
                             @PathVariable(name = "productId") Long productId) {
        Optional<Product> optionalProduct = productService.findById(productId);
        List<ProductPhoto> productPhotos;
        if (optionalProduct.isPresent()) {
            Product product = optionalProduct.get();
            productPhotos = product.getPhotos();

            model.addAttribute("product", product);
            model.addAttribute("photos", productPhotos);
            model.addAttribute("username", principal.getName());
        }

        return "photo-list";
    }

    @PostMapping("/addPhoto")
    public String addPhoto (Long productId, MultipartFile photo) {
        try {
            productService.addPhoto(productId, photo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:/product/" + productId + "/photos";
    }


    @GetMapping("/search")
    public String searchForm(Model model, SearchRequest dto) {
        model.addAttribute("searchDto", dto);
        model.addAttribute("categories", ProductCategory.values());

        return "search-form";
    }

    @PostMapping("/search")
    public String searchProducts(SearchRequest dto, Model model, Principal principal) {
        List<Product> products = productService.getAllFromDto(dto);
        if (!products.isEmpty()) {
            model.addAttribute("products", products);
            model.addAttribute("ownerName", principal.getName());

            return "product-list";
        }
        return "redirect:/";
    }

}
