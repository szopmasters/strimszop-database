package pl.strimszop.cloud.strimszopdatabase.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import pl.strimszop.cloud.strimszopdatabase.service.InvoiceService;

@Controller
@AllArgsConstructor
public class InvoiceController {
    private final InvoiceService invoiceService;
}
