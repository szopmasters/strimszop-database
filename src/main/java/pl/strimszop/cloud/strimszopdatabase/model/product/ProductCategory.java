package pl.strimszop.cloud.strimszopdatabase.model.product;

public enum ProductCategory {
    UBRANIA,
    KOSMETYKI,
    ELEKTRONIKA,
    MOTORYZACJA,
    GRY,
    FILMY,
}
