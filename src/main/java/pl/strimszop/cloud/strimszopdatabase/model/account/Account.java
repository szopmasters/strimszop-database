package pl.strimszop.cloud.strimszopdatabase.model.account;

import lombok.*;
import org.hibernate.annotations.Cascade;
import pl.strimszop.cloud.strimszopdatabase.model.product.Invoice;
import pl.strimszop.cloud.strimszopdatabase.model.product.Product;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    private boolean isDisabled;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @Cascade(value = org.hibernate.annotations.CascadeType.DETACH)
    private Set<AccountRole> accountRoles;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany(mappedBy = "subscribees")
    private Set<Account> subscribers;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany
    private Set<Account> subscribees;

    @OneToMany(mappedBy = "owner")
    private List<Product> products;

    @OneToMany(mappedBy = "buyer")
    private Set<Invoice> invoices;

    public boolean isAdmin() {
        return accountRoles.stream().anyMatch(ar -> ar.getName().equals("ADMIN"));
    }
}

