package pl.strimszop.cloud.strimszopdatabase.model.specification;

import org.springframework.data.jpa.domain.Specification;
import pl.strimszop.cloud.strimszopdatabase.model.product.Product;
import pl.strimszop.cloud.strimszopdatabase.model.product.dto.SearchRequest;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class ProductSpecification implements Specification<Product> {
    private SearchRequest searchRequest;

    public ProductSpecification(SearchRequest searchRequest) {
        this.searchRequest = searchRequest;
    }

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> criteria, CriteriaBuilder builder) {
        Predicate finalPredicate = builder.between(root.get("price"), searchRequest.getFrom(), searchRequest.getUpTo());

        if (searchRequest.getName() != null && !searchRequest.getName().isEmpty()) {
            finalPredicate = builder.and(finalPredicate, builder.like(root.get("name"), "%" + searchRequest.getName() + "%"));
        }
        if (searchRequest.getCategory() != null) {
            finalPredicate = builder.and(finalPredicate, builder.equal(root.get("category"), searchRequest.getCategory()));
        }
        return finalPredicate;
    }
}
