package pl.strimszop.cloud.strimszopdatabase.model.account.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.strimszop.cloud.strimszopdatabase.model.product.Product;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreatePhotoRequest {
    private Long id;
    private byte[] photo;
    private Product product;
}
