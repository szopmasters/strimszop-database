package pl.strimszop.cloud.strimszopdatabase.model.product;

import lombok.*;

import javax.persistence.*;
import java.util.Base64;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ProductPhoto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    @Column(name = "photo", columnDefinition = "LONGBLOB")
    private byte[] photo;

    @Column(columnDefinition = "TINYINT DEFAULT 0")
    private boolean isPrimary;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne
    private Product product;

    public String convertBinImageToString() {
        if (photo!=null) {
            return Base64.getEncoder().encodeToString(photo);
        }
        return "";
    }
}
