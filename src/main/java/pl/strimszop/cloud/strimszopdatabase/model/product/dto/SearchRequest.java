package pl.strimszop.cloud.strimszopdatabase.model.product.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.strimszop.cloud.strimszopdatabase.model.product.ProductCategory;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchRequest {
    private String name;
    private ProductCategory category;
    private Double from = 0D;
    private Double upTo = 10000D;
}