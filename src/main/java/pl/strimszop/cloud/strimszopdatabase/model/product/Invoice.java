package pl.strimszop.cloud.strimszopdatabase.model.product;

import lombok.*;
import org.hibernate.annotations.Formula;
import pl.strimszop.cloud.strimszopdatabase.model.account.Account;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @Formula(value = "(select (product.price*purchase.product_quantity) from purchase join product on product.id=purchase.product_id where invoice_id=id)")
//    private Double totalSum;

    @Column(columnDefinition = "tinyint default 0")
    private boolean paid;

    private LocalDateTime dateOfPurchase;

    private LocalDateTime dateOfPayment;

    @ManyToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Account buyer;

    @OneToMany(mappedBy = "invoice", fetch = FetchType.EAGER)
    private List<Purchase> purchases;
}