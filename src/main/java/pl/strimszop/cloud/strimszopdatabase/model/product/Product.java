package pl.strimszop.cloud.strimszopdatabase.model.product;

import lombok.*;
import org.hibernate.annotations.Cascade;
import pl.strimszop.cloud.strimszopdatabase.model.account.Account;

import javax.persistence.*;
import java.util.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    private double price;

    @Enumerated(value = EnumType.STRING)
    private ProductCategory category;

    private int quantity;

    @OneToMany(mappedBy = "product", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE)
    private List<ProductPhoto> photos = new ArrayList<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne
    private Account owner;

    @OneToMany(mappedBy = "product", fetch = FetchType.EAGER)
    private Set<Purchase> purchases;

    public Product(String melko) {
        this.name = melko;
    }

    public String convertBinImageToString() {
        if (!photos.isEmpty()) {
            Optional<ProductPhoto> optionalFirstPhoto = photos.stream().filter(ProductPhoto::isPrimary).findFirst();
            if (optionalFirstPhoto.isPresent()) {
                byte[] firstPhoto = optionalFirstPhoto.get().getPhoto();
                return Base64.getEncoder().encodeToString(firstPhoto);
            }
        }
        return "";
    }
}